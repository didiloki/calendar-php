<?php

/**
 * User: Iweala Ebere
 * Date: 25/11/2015
 * Time: 06:51
 */
class Calender
{
    public $day;
    public $month;
    public $year;

    private $start_year;
    private $end_year;

    private $monthName;
    private $YearNumber;
    private $no_of_days;

    public $daysOfMonth;

    private $prevMonth;
    private $nextMonth;

    private $datePrevMonth;
    private $dateNextMonth;

    private $displayCalender;


    public function __construct()
    {
        $this->month = date('m');// Current month as default month
        $this->year = date('Y');// Current month as default year

        $this->start_year = $this->year; //set default start year to current year
        $this->end_year = $this->start_year + 10; // end year at 10 years from start year

        $this->prevMonth= $this->month-1;
        $this->nextMonth= $this->month+1;

    }

    public function setMonth($month)
    {
        if(!($month < 13 and $month > 0)){
            $this->month =date("m");
        }
        else{
            $this->month = $month;
        }
    }

    public function getMonth()
    {
        return $this->month;
    }

    public function getMonthName()
    {
        for($p=1;$p<=12;$p++){

            $dateObject   = DateTime::createFromFormat('!m', $p);
            $monthName = $dateObject->format('F');
            if($this->getMonth()==$p){
                echo "<option value=$p selected>$monthName</option>";
            }else{
                echo "<option value=$p>$monthName</option>";
            }
        }
        return $this->monthName;
    }

    public function setYear($year)
    {
        if(!($year <= $this->end_year && $year >= $this->start_year)){
            $this->year = date("Y");  // Set current year as default year
        }else{
            $this->year = $year;
        }
    }


    public function getYear()
    {
        return $this->year;  // Set current year as default year

    }

    public function getYearNumber()
    {
        for($i=$this->start_year; $i<=$this->end_year; $i++){
            if($this->getYear()==$i){
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'> $i</option>";
            }
        }
        return $i;
    }

    public function getDay()
    {
        return $this->day;
    }

    public function getNoOfDays()
    {
        return cal_days_in_month(CAL_GREGORIAN, $this->getMonth(), $this->getYear());//calculate number of days in a month;
    }

    public function getNoOfDaysInPrevMonth($month, $year)
    {
        return cal_days_in_month(CAL_GREGORIAN, $month, $year);//calculate number of days in a month;
    }

    public function getNextMonth()
    {
        $month= $this->getMonth() + 1; //increment month by 1 to go to next
        $year  = $this->getYear();

        if ($month == 0 ) {
            $month= 12;
            $year = $year - 1;
        }
        if ($month == 13 ) {
            $month = 1;
            $year = $year + 1;
        }

        $this->nextMonth = "<a class=\"button\" href=\"";
        $this->nextMonth .= "?month=". $month . "&year=" . $year ."\">Next Month<span class=\"glyphicon glyphicon-arrow-right\" aria-hidden=\"true\"></span>";
        $this->nextMonth .= "</a>";

        return $this->nextMonth;
    }


    public function getPrevMonth()
    {
        $month = $this->getMonth() - 1; //decrement month by 1 to go to next
        $year  = $this->getYear();

        if ($month == 0 ) {
            $month= 12;
            $year = $year - 1;
        }
        if ($month == 13 ) {
            $month = 1;
            $year = $year + 1;
        }

        $this->prevMonth  = "<a href=\"";
        $this->prevMonth .= "?month=". $month . "&year=" . $year ."\" ><span class=\"glyphicon glyphicon-arrow-left\" aria-hidden=\"true\"></span>Previous Month";
        $this->prevMonth .= "</a>";

        return $this->prevMonth;
    }

    public function datesPrevMonth($count){

        //$count = 6;
        $cur_month = $this->getMonth() - 1;
        if($cur_month < 1){ $cur_month = 12;}
        $cur_year  = $this->getYear() - 1;

        $Tabs = $count - 2; //print only 2

        $dayOfPrevMonth = ($this->getNoOfDaysInPrevMonth($cur_month, $cur_year) - $count) + 1;

        $this->datePrevMonth = " ";
        if($count >= 0){
            for($i = 0; $i < $count; $i++ ){
                //print empty till only 2 left
                if($i < $Tabs ){
                    $this->datePrevMonth .= "<td style='background-color: aliceblue;'>" ." </td>";
                    if($dayOfPrevMonth != $this->getNoOfDaysInPrevMonth($cur_month,$cur_year)){
                        $dayOfPrevMonth = $dayOfPrevMonth + 1;

                    }

                }else{
                    $this->datePrevMonth .= "<td style='background-color: aliceblue;'>". $dayOfPrevMonth ." </td>";
                    $dayOfPrevMonth = $dayOfPrevMonth + 1;
                }
            }
        }
        return $this->datePrevMonth;

    }

    public function datesNextMonth($count){

        $this->dateNextMonth = "";
        $print = 1;

        /* since its a 6 x 7 table 6 x 7 = 42
         *  so count the rows and columns
         * and subtract from days n count
        */
        if($count >= 0){
            $blank_spaces = 42 - $count - $this->getNoOfDays(); // last blanks of the month
            if($blank_spaces >= 7){$blank_spaces = $blank_spaces - 7 ;}

            /* print counter till blankspaces are full */
            for($i = 1; $i <= $blank_spaces; $i++ ){
                /* print only 2 */
                if($print < 3){
                $this->dateNextMonth.= "<td style='background-color: aliceblue;'>". $print ." </td>";
                $print++;
                }
                else{
                    $this->dateNextMonth.= "<td style='background-color: aliceblue;'>"." </td>";
                }
            }
        }
        return $this->dateNextMonth;

    }

    public function getCalender()
    {
        $this->displayCalender = "<table class=\"table table-condensed\">";

        //Print days of the week
        $this->displayCalender .= "<tr><th>Monday</th><th>Tuesday</th><th>Wednesday</th><th>Thursday</th><th>Friday</th><th>Saturday</th><th>Sunday</th></tr><tr>";

        // Get first day of month
        // Sun = 0 , Sat = 6
        $j= date('w',mktime(0,0,0,$this->getMonth(),1,$this->getYear()));

        //if first day of the week is Monday
        $j=$j-1;

        if($j<0)
        {
            $j=6;
        }  // if it is Sunday //

        // end of if starting day of the week is Monday ////

        $prevDates = $this->datesPrevMonth($j); // get the days from the months
        $nextDates = $this->datesNextMonth($j);

        // Start
        for($i=1; $i<=$this->getNoOfDays(); $i++){

            $this->displayCalender .= $prevDates." <td>$i"; // display the date in cell
            $this->displayCalender .= " </td>";

            $prevDates='';

            $j++;

            if($j==7){
                $this->displayCalender .= "</tr><tr>"; //new row
                $j=0;
            }

        }
        $this->displayCalender .= $nextDates;
        $this->displayCalender .= "</table>";

        return $this->displayCalender;
    }
}