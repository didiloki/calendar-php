<? include 'Calender.php';
$fo = new Calender();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PHP Calender</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-aUGj/X2zp5rLCbBxumKTCw2Z50WgIr1vs/PFN4praOTvYXWlVyh2UtNUU0KAUhAX" crossorigin="anonymous">

    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>

</head>
<body>

<script type="text/javascript">

    function refresh(formData){
        var valMonth = document.getElementById('month').value; // collect month value
        var valYear  = document.getElementById('year').value;      // collect year value

        self.location = '?month=' + valMonth + '&year=' + valYear ; // reload the page
    }
</script>

<div class="container" style="padding-top: 50px;">

    <div class="col-md-6">
        <?php

        $urlMonth = $_GET['month'];
        $urlYear = $_GET['year'];

        $fo->setMonth($urlMonth);
        $fo->setYear($urlYear);

        ?>
        <div class="col-md-8">
            <div class="col-md-5">
                <div class="form-group">
                <select class="form-control" name="month" onchange="refresh(this.formData)" id="month">
                    <option value=''>Select Month</option>

                    <?php  $fo->getMonthName(); ?>
                </select>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
            <select class="form-control" name="year" onchange="refresh(this.formData)" id="year">
                <option value=""> Select Year</option>

                <? $fo->getYearNumber(); ?>
            </select>

            </div>
        </div>
        </div>


    <div class="col-md-8">
        <?php
        //display calender
        echo '<br>' . $fo->getCalender();

        //display navigation
        echo '<div class="pull-right">' . $fo->getPrevMonth() . ' | ' . $fo->getNextMonth() . '</div>';




        ?>
    </div>
</div>
</div>
</body>
</html>